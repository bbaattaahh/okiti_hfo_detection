from src.do_parallel import do_parallel


def test_working():
    # given
    def f(x, arg1):
        return x * arg1

    iterable_variable = [1, 2, 3, 4]

    arg1 = [2, 2, 2, 2]

    expected_result = [2, 4, 6, 8]

    # when
    actual_result = do_parallel(f, iterable_variable, arg1)

    # that
    assert actual_result == expected_result
