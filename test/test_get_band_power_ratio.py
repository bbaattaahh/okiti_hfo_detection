import matplotlib.pyplot as plt
import scipy.signal
from src.get_band_power_ratio import get_band_power_ratio
from src.fraquency2scale import frequency2scale

def test_get_band_power_ratio_working():
    # given
    # # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.morlet.html
    # # Highlight: The fundamental frequency of this wavelet in Hz is given by
    # # f = 2*s*w*r / M where r is the sampling rate. -> s = (f*M) / (2*w*r)
    central_frequency = 150
    w = 10
    M = 401
    r = 2000
    s = (central_frequency*M) / (2*w*r)

    signal = scipy.signal.morlet(M, w=w, s=s, complete=False)
    sampling_period = 1/r

    frequency_band_1 = (100, 200)
    frequency_band_2 = (400, 500)
    wavelet = 'cmor'

    plt.plot(signal)
    plt.show()

    expected_band_power = 3.2

    # when
    actual_band_powers = get_band_power_ratio(frequency_band_1,
                                              frequency_band_2,
                                              signal,
                                              sampling_period,
                                              wavelet)

    # that
    assert actual_band_powers > expected_band_power
