from src.get_ev2_files_difference import get_ev2_files_diff


def test__get_ev2_files_difference():
    # given
    ev2_1 = "./test/data/get_ev2_file_difference__all.ev2"
    ev2_2 = "./test/data/get_ev2_file_difference__pos.ev2"



    expected_event_positions = [(0, 4), (1, 2)]

    # when
    actual_event_positions = get_ev2_files_diff(ev2_1, ev2_2)

    # that
    assert actual_event_positions == expected_event_positions
