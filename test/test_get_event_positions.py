from src.get_event_positions import get_event_positions


def test__get_column_number_working():
    # given
    ev2_file_name = "./test/data/get_event_positions.ev2"

    expected_event_positions = [(0, 4), (1, 2)]

    # when
    actual_event_positions = get_event_positions(ev2_file_name)

    # that
    assert actual_event_positions == expected_event_positions
