import numpy as np

from src.get_peaks import get_peaks
from src.Peak import Peak


def test_get_peaks_working():
    # given
    eeg_part = np.array([0, 0, 2, 3, 2, 1, 0, 1, 2, 3, 0, 0])
    threshold = 1

    peak_1 = Peak(start_index=2,
                  end_index=5,
                  values=np.array([2, 3, 2]),
                  threshold=1)

    peak_2 = Peak(start_index=8,
                  end_index=10,
                  values=np.array([2, 3]),
                  threshold=1)

    expected_peaks = [peak_1, peak_2]

    # when
    actual_peaks = get_peaks(eeg_part, threshold)

    # that
    assert actual_peaks[0] == expected_peaks[0]
