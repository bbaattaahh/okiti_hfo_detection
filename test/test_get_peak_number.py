import numpy as np

from src.get_peak_number import get_peak_number


def test_get_peak_number_working():
    # given
    eeg_part = np.array([0, 0, 2, 3, 2, 1, 0, 1, 2, 3, 0, 0])

    threshold_function = lambda x: 1

    expected_peak_number = 2

    # when
    actual_peak_number = get_peak_number(eeg_part, threshold_function)

    # that
    assert actual_peak_number == expected_peak_number
