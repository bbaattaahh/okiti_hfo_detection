import pywt
import numpy as np
from src.fraquency2scale import frequency2scale


def test_frequency2scale_working():
    # given
    frequencies = range(10, 1000)
    wavelet = "morl"
    sampling_period = 1/2000

    for frequency in frequencies:
        # when
        actual_frequency = pywt.scale2frequency(wavelet=wavelet,
                                                scale=frequency2scale(frequency, wavelet, sampling_period)) / sampling_period

        # that
        np.testing.assert_almost_equal(actual_frequency, frequency, decimal=5)


def test_frequency2scale_cmor():
    # given
    frequencies = range(10, 1000)
    wavelet = "cmor"
    sampling_period = 1/2000

    for frequency in frequencies:
        # when
        actual_frequency = pywt.scale2frequency(wavelet=wavelet,
                                                scale=frequency2scale(frequency, wavelet, sampling_period)) / sampling_period

        # that
        np.testing.assert_almost_equal(actual_frequency, frequency, decimal=5)