import unittest

import numpy as np
import json

from collections import OrderedDict
from src.DataSetGenerator import DataSetGenerator


class TestDataSetGenerator(unittest.TestCase):

    def test_generate_data_set_working(self):
        # given
        data_set_map = json.loads(
            """{"./test/data/HFO_minta_block.cnt": {
                    "positive": ["./test/data/HFO_minta_block_small_pos.ev2",
                                  "./test/data/HFO_minta_block_small_pos_2.ev2"],
                    "negative": ["./test/data/HFO_minta_block_small_neg.ev2"]
                    }
            }""",
            object_pairs_hook=OrderedDict)


        dsg = DataSetGenerator(window_size=3, data_set_map=data_set_map)
        dsg.generate_data_set()

        expected_targets = ["positive", "positive", "positive", "positive",
                            "positive", "positive", "negative", "negative"]

        expected_cnt_file_names = ["./test/data/HFO_minta_block.cnt"] * 8

        expected_ev2_file_names = ["./test/data/HFO_minta_block_small_pos.ev2",
                                   "./test/data/HFO_minta_block_small_pos.ev2",
                                   "./test/data/HFO_minta_block_small_pos.ev2",
                                   "./test/data/HFO_minta_block_small_pos_2.ev2",
                                   "./test/data/HFO_minta_block_small_pos_2.ev2",
                                   "./test/data/HFO_minta_block_small_pos_2.ev2",
                                   "./test/data/HFO_minta_block_small_neg.ev2",
                                   "./test/data/HFO_minta_block_small_neg.ev2"]

        expected_ev2_file_row_indices = [1, 2, 3, 1, 2, 3, 1, 2]

        expected_attribute_shape = (8, 3)

        # when
        actual_targets = dsg.targets
        actual_cnt_file_names = dsg.cnt_file_names
        actual_ev2_file_names = dsg.ev2_file_names
        actual_ev2_file_row_indices = dsg.ev2_file_row_indices
        actual_attribute_shape = dsg.attributes.shape

        # that
        assert actual_targets == expected_targets
        assert actual_cnt_file_names == expected_cnt_file_names
        assert actual_ev2_file_names == expected_ev2_file_names
        assert actual_ev2_file_row_indices == expected_ev2_file_row_indices
        assert actual_attribute_shape == expected_attribute_shape

    def test__zip_attributes_to_numpy_working(self):
        # given
        attributes = [[np.array([0, 0, 0]), np.array([1, 1, 1])],
                      [np.array([2, 2, 2])],
                      [np.array([3, 3, 3]), [4, 4, 4]]]

        expected_attributes = np.array([[0, 0, 0],
                                        [1, 1, 1],
                                        [2, 2, 2],
                                        [3, 3, 3],
                                        [4, 4, 4]])

        # when
        actual_attributes = DataSetGenerator.zip_attributes_to_numpy(attributes)

        # that
        assert np.array_equal(actual_attributes, expected_attributes)

    def test_attributes_to_pdf_working(self):
        # given
        data_set_map = json.loads(
            """{"./test/data/HFO_minta_block.cnt": {
                    "positive": ["./test/data/HFO_minta_block_small_pos.ev2",
                                  "./test/data/HFO_minta_block_small_pos_2.ev2"],
                    "negative": ["./test/data/HFO_minta_block_small_neg.ev2"]
                    }
            }""",
            object_pairs_hook=OrderedDict)

        dsg = DataSetGenerator(window_size=501, data_set_map=data_set_map)
        dsg.generate_data_set()
        dsg.attributes_to_pdf()

        expected_attributes = True

        # when
        actual_attributes = False

        # that
        assert np.array_equal(actual_attributes, expected_attributes)
