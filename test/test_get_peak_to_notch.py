import matplotlib.pyplot as plt
import scipy.signal
from src.get_peak_to_notch import get_peak_to_notch
from src.fraquency2scale import frequency2scale


def test_get_peak_to_notch_working():
    # given
    scale = frequency2scale(frequency=100, wavelet="cmor", sampling_period=1/2000)

    frequency_band = (40, 500)
    signal = scipy.signal.morlet(401, s=scale, complete=True)
    sampling_period = 1/2000
    wavelet = 'cmor'

    plt.plot(signal)
    plt.show()

    expected_peak_to_notch = True

    # when
    actual_peak_to_notch = get_peak_to_notch(signal,
                                           frequency_band,
                                           wavelet,
                                           sampling_period)

    # that
    assert actual_peak_to_notch == expected_peak_to_notch
