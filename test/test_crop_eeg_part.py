import numpy as np

from src.crop_eeg_parts import crop_eeg_parts, _is_it_ev2_file


def test_crop_eeg_parts_working_with_ev2_file_input():
    # given
    event_positions = "./test/data/test_crop_eeg_parts.ev2"
    eeg_data = np.array([[0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
                         [0, 2, 2, 2, 0, 0, 0, 0, 0, 0]])
    window_size = 3

    expected_eeg_parts = [np.array([1, 1, 1]), np.array([2, 2, 2])]

    # when
    actual_eeg_parts = crop_eeg_parts(event_positions, eeg_data, window_size)

    # that
    assert np.array_equal(actual_eeg_parts, expected_eeg_parts)


def test_crop_eeg_parts_working_with_position_data_input():
    # given
    event_positions = [(0, 4), (1, 2)]
    eeg_data = np.array([[0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
                         [0, 2, 2, 2, 0, 0, 0, 0, 0, 0]])
    window_size = 3

    expected_eeg_parts = [np.array([1, 1, 1]), np.array([2, 2, 2])]

    # when
    actual_eeg_parts = crop_eeg_parts(event_positions, eeg_data, window_size)

    # that
    assert np.array_equal(actual_eeg_parts, expected_eeg_parts)


def test__is_it_ev2_file__ev2_file_name_input():
    # given
    event_positions = "./test/data/test_crop_eeg_parts.ev2"

    expected_flag = True

    # when
    actual_flag = _is_it_ev2_file(event_positions)

    # that
    assert actual_flag == expected_flag


def test__is_it_ev2_file__defined_positions_input():
    # given
    event_positions = [(0, 4), (1, 2)]

    expected_flag = False

    # when
    actual_flag = _is_it_ev2_file(event_positions)

    # that
    assert actual_flag == expected_flag