# OKITI HFO DETECTION

## HOW TO RUN



## SUBSCRIPTS
### Generate Data Set (_generate_data_set.py_)


## FEATURES
### f1_max
Maximum value

### f2_std
Standard deviation.
https://docs.scipy.org/doc/numpy/reference/generated/numpy.std.html

### f3_entropy
https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.entropy.html

### f4_peak_number
Number of beaks.

### f5_peak_maximums_avg
The mean of the max value of peaks

### f6_power_band_ratio_50_150_VS_500_600
1. Wavelet transformation
2. Power specteral density matrix
3. Get two band of frequencies of Power specteral density matrix
4. Get maximum values of the two bands
5. Get their ratio (max of lower frequency band/max of higher frequency band )

### f7_peak_to_notch
See the details in these files:
1. peak-to-notch.pptx
2. peak-to-notch_jav_lépés.pptx
You can find them in the __ducumentation__ folder, they are written by Virág Bokondi.

## HOW TO GIT
1. checkout (e.x. feature/new_branch)
2. do my programing
3. checkout master
4. pull
5. checkout (e.x. feature/new_branch)
6. master -> "Merge into Current"
7. resolve merge conflicts (you have to add again the conflicted files)
8. Commit
9. Push
10. In Bitbucket web page https://bitbucket.org/bbaattaahh/okiti_hfo_detection/branches/  "...-> Merge -> Merge"
