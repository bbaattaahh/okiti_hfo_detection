import pickle
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn import tree
import pydotplus
import collections


with open('data_set.pkl', 'rb') as f:
    df = pickle.load(f)


#split dataset in features and target variable
target_column_name = "targets"

column_names = df.columns.values
not_feature_columns = ['attributes', 'cnt_file_names', 'ev2_file_names', 'ev2_file_row_indices', 'targets']
feature_cols = list(set(column_names) - set(not_feature_columns))


X = df[feature_cols] # Features
y = df[target_column_name] # Target variable



# Create Decision Tree classifer object
clf = DecisionTreeClassifier()

scores = cross_val_score(clf, X, y, cv=5)

print(scores)

print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))






clfs = []

for i in range(5):
    # Split dataset into training set and test set
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=i)


    # Create Decision Tree classifer object
    clf = DecisionTreeClassifier()

    # Train Decision Tree Classifer
    clf = clf.fit(X_train,y_train)

    clfs.append(clf)

#Predict the response for test dataset
y_pred = clf.predict(X_test)



# Visualize data
dot_data = tree.export_graphviz(clf,
                                feature_names=X.columns.values,
                                out_file=None,
                                filled=True,
                                rounded=True)
graph = pydotplus.graph_from_dot_data(dot_data)

colors = ('turquoise', 'orange')
edges = collections.defaultdict(list)

for edge in graph.get_edge_list():
    edges[edge.get_source()].append(int(edge.get_destination()))

for edge in edges:
    edges[edge].sort()
    for i in range(2):
        dest = graph.get_node(str(edges[edge][i]))[0]
        dest.set_fillcolor(colors[i])

graph.write_png('./documentation/tree.png')

print(1)
