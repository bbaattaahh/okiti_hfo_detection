# OKITI HFO DETECTION - ÖSSZEFOGLALÁS

## CÉLKITŰZÉS
OKITI HFO szakértőinek, manuális HFO válogatásának kiváltása, felgyorsítása.

## ADATSZET

OKITI-ből származó flag-elt, adatszet. 38 db EEG feltétel és ahozzájuk tartozó HFO pozíciók.
Adat fájlok (n futó index):
1. minta\_n\_.cnt: nyers EEG felvételek
2. minta\_n\_automata.ev2 : OKITI saját algoritmusa áltlá FHO-nak jelölt   esemény közepek
3. minta\_n\_manualis.ev2: OKITI szakértő által HFO-nak ítelet esémnyközepek,(A minta\_n\_manualis.ev2 a minta_n_automata.ev2-nek a volódi részhalmaza.)

Jelen projecktben csak egy adatszet hármassal foglalkoztunk, a 38-as számúval, de a többi meglévő adatszetre is könnyedén kiterjeszthető az elemzésünk.

## ADAT FELDOLGOZÁS

1. A minta\_n\_automata.ev2-nek és a minta\_n\_manualis.ev2 fájlokból meghatározzuk a pozitív és a negatív mintákat. (Előállítjuk a célváltozót.)
2. Az OKITI-ben Matlab kód segítségével generáljár a cnt fájlokat trc fájlokból. A konverzió során az adatszetek hosszát rosszúl határozzák meg és rossz értéket mentenek a cnt fájl header részébe. Ennek következtében a mne python package nem tudja beolvasni az adatokat. Ennek javítására írtunk egy python package-et, ami javítja az cnt header-t. (MNE package számos előre megírt EEG adatfeldolgozó algoritmust tartalmaz.)
3. 1-5 faturokig az EEG az alábbi feldolgozásokon esik át.
	- FIR band-pass filterrel történő frekvencia szűrés (MNE beépített függvény)
	- Savitzky–Golay filterrel való símítás
	- EEG értékek abszolút értékét vesszük
4. 6-7-es feature-ök nem esnek át feldolgozáson.
5. A cnt fájlokből kivágjuk a HFO-nak ítélt EEG részleteket.
6. Ezt követően a EEG részletekre előállítjuk a feature-ket.


## FEATURE-ÖK
### Feature 1, maxium (f1\_max)
Vesszük az adott EEG szakasz maximális értékét.

### Feature 2, szórás (f2\_std)
Vesszük az adott EEG szakasz szórását.

### Feature 3, (f3\_entropy)
Vesszük az adott EEG szakasz entrópiáját.

### Feature 4, (f4\_peak\_number)
A csúcsok meghatározásához szükséges küszöbértéket az EEG szakasz szorásának mértékében határoztuk meg. (Jelenleg a szórás 3 szorosa.) Ezután számoljuk a keletkezett csúcsok számát.

### Feature 5, (f5\_peak\_maximums\_avg)
A csúcsok meghatározásához szükséges küszöbértéket az EEG szakasz szorásának mértékében határoztuk meg. (Jelenleg a szórás 3 szorosa.) Ezután a keletkezett csúcsok maximális értékének átlagát vesszük.


### Feature 6, (f6\_power\_band\_ratio\_50\_150\_VS\_500\_600

1. Komplex Wavelet transzformációt hajtunk végre az EEG szakaszon,
2. Majd ebből előállítjuk a power specteral density mátrixot
3. A power spectral density mátrixból hivágunk két frekvencia tartományt,
4. A frekvencia tartományokban külön külön megkeressük a maximális értékeket,
5. A két maximális érték hányadosát vesszük, úgy hogy a kisebb frekvencia tartományhoz tartozó érteék van a számlálóban.

A feature az alábbi cikk alapján készült: __Jahidin\_2012\_IEEE\_Brainwave sub-band power ratio characteristics in intelligence assessment.pdf__. A pdf megtalálható a __dokumentation__ mappában.

### Feature 7, (f7\_peak\_to\_notch)

1. Komplex Wavelet transzformációt hajtunk végre az EEG szakaszon,
2. Majd ebből előállítjuk a power specteral density mátrixot
3. Frekvenciánként átlagoljuk a power specteral density mátrixot
4. 40 és 80 Hz között megkeresskük az átlagok között a minimumot
5. Ettől a minimum helytől nagyobb frekvencia értékeknél maximumokat keresunk.
6. A maximumok helyei és maximumok helyi minusz 50 Hz-es tartományokban minimum értéket keresünk.
7. Kiszámítjuk a maximum értékek és minimum értékek hányadosait és vesszük ezek közül a legnagyobbat.

A feature részletes leírását az alábbi csatolmányok tartalmazzák:

1. peak-to-notch.pptx, peak-to-notch_jav_lépés.pptx (Alábbi cikk kivonata.)
2. A feature az alábbi cikk alapján készült: __Liu\_2016\_JNeuralEng\_Exploring the time-frequency content of high frequency oscillations for automated identification of seizure onset zone in epilepsy.pdf__

A fájlok a __ducumentation__ mabbában találhatóak meg.


## MODEL ÉPÍTÉS
Döntési fa algoritmust alkalmaztunk az elkészült feature-ökre. Az adatszetet cross validaton-nel futtatuk, úgy hogy az adatszetet öt részre bontottuk. A tréning szeten az átlagos pontosság 99% volt, és a szórása 1%.
Azonban meg kell jegyeni, hogy a mintaként szolgált 38as adatszet, szemrevételezés során kiemelkedően jó minőségűnek itéltük, illetve a számos további számítást kell annak érdekében végezni, hogy hogy egyértelmüen kijelenthessük, hogy jó modelt sikerült építenünk. Ezekrők részletesebben a __TOVÁBBI TERVEK__ pontban írunk.


## KONKLÚZIÓ
A projekt során kidolgoztunk egy metódust, amelynek eredménye képpen képesek vagyunk az OKIT-ben kelettkezett eremények felhasználásával, olyan gépi tanulás modelt építeni, ami segítheti és/vagy kiválthatjó az OKITI munkatársainak munkáját.
A megoldásunk a HFO-t tartalmazó (vagy annak vélt) EEG részletekre feature-öket képes számítani, valamint a feature-ökre gépi tanulási algoritmusokat futtatni.
A megoldás elkészítése sorám különös figyelmet fordítottunk arra, hogy a jövőben könnyen lehet bővíteni az eljárásunkat, további EEG adatokkal, feature-ökkel mind pedig gépi tanulás algoritmusokkal.

## TOVÁBBI TERVEK
1. Model építésnél a "class imbalance" foglalkozni, mivel az adatszet sokkkal több (kb. 10szer annyi) negatív mintát tartalmaz, mint pozitívat. Ezért az alábbi értékeket szeretnénk gorcső alá venni:
	- precision és recall értékek,
	- POC és Lift görbék a cutoff pontok jóságának meghatározására,
	- confusion matrix
2. Minden EEG fájlra kiterjeszteni a futtatást.
3. További feature-ök implementálása.
4. Más gépi tanulás algoritmus alkalmazása.
