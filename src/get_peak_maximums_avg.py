import numpy as np

from src.get_peaks import get_peaks


def get_peak_maximums_avg(eeg_part, threshold_function):
    threshold = threshold_function(eeg_part)

    peaks = get_peaks(eeg_part, threshold)

    maximum_peak_values = [peak.max_value for peak in peaks]

    peak_maximums_avg = np.mean(maximum_peak_values)

    return peak_maximums_avg
