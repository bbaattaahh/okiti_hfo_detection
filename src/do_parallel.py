import multiprocessing


def do_parallel(f, attribute, *args):

    print(args[0])

    x = list(map(f, attribute, args[0]))
    y =_pool_number_to_start()

    pool = multiprocessing.Pool(_pool_number_to_start())
    result = pool.map(f, attribute, args[0])
    pool.close()
    pool.join()

    return result


def _pool_number_to_start():
    core_number = multiprocessing.cpu_count()

    if core_number < 2:
        return core_number

    return core_number - 1
