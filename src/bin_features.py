import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from itertools import compress



def bin_features(feature, target, bin_number=10):

    categories = pd.qcut(np.array(feature), q=bin_number, duplicates="drop")
    negative_numbers = []
    positive_numbers = []
    bin_names = []


    for index in range(bin_number):
        x = categories._codes==index
        act_targets = list(compress(target, x))

        act_positive_number = act_targets.count("positive")
        positive_numbers.append(act_positive_number)
        act_negative_number = act_targets.count("negative")
        negative_numbers.append(act_negative_number)
        act_bin = str(categories[index])
        bin_names.append(act_bin)


    df = pd.DataFrame(data={"pos":positive_numbers,
                            "neg":negative_numbers,
                            "bin_label":bin_names})

    return df