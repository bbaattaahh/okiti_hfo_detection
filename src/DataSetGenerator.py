import numpy as np
import pandas as pd


from mne.io.cnt import read_raw_cnt
from okiti_cnt_to_cnt import okiti_cnt_to_cnt
from matplotlib.backends.backend_pdf import FigureCanvasPdf, PdfPages
from matplotlib.figure import Figure

from src.crop_eeg_parts import crop_eeg_parts


class DataSetGenerator:
    def __init__(self,
                 window_size=501,
                 data_set_map=None,
                 cnt_process_function=None,
                 whole_channel_process_function=None,
                 eeg_parts_process_function=None):

        self.window_size = window_size
        self.data_set_map = data_set_map
        self.cnt_process_function = cnt_process_function
        self.whole_channel_process_function = whole_channel_process_function
        self.eeg_parts_process_function = eeg_parts_process_function
        self.targets = None
        self.attributes = None
        self.cnt_file_names = None
        self.ev2_file_names = None
        self.ev2_file_row_indices = None
        self.frequency = 2000

    def generate_data_set(self):
        cnt_file_names_to_save = []
        ev2_file_names_to_save = []
        ev2_file_row_indices = []
        attributes = []
        targets = []

        # positive samples
        for cnt_file_name, target_types in self.data_set_map.items():
            okiti_cnt_to_cnt(cnt_file_name)
            raw_cnt = read_raw_cnt(cnt_file_name, montage=None, preload=True)
            if self.cnt_process_function is not None:
                processed_cnt = self.cnt_process_function(raw_cnt)
            else:
                processed_cnt = raw_cnt

            eeg_data = processed_cnt.get_data()

            if self.whole_channel_process_function is not None:
                row_number = eeg_data.shape[0]
                column_number = self.get_column_number(self.whole_channel_process_function, eeg_data[0, :])
                processed_eeg_data = np.empty(shape=(row_number, column_number), dtype=np.float64)

                for row_index in range(eeg_data.shape[0]):
                    processed_eeg_data[row_index, :] = self.whole_channel_process_function(eeg_data[row_index, :])
            else:
                processed_eeg_data = eeg_data

            for act_target_type, ev2_file_names in target_types.items():
                for ev2_file_name in ev2_file_names:
                    eeg_parts = crop_eeg_parts(ev2_file_name, processed_eeg_data, self.window_size)
                    if self.eeg_parts_process_function is not None:
                        processed_eeg_parts = list(map(self.eeg_parts_process_function, eeg_parts))
                    else:
                        processed_eeg_parts = eeg_parts

                    attributes.append(processed_eeg_parts)

                    targets.extend([act_target_type] * len(eeg_parts))
                    ev2_file_names_to_save.extend([ev2_file_name] * (len(eeg_parts)))
                    ev2_file_row_indices.extend(list(range(1, len(eeg_parts)+1)))
                    cnt_file_names_to_save.extend([cnt_file_name] * len(eeg_parts))

        self.attributes = self.zip_attributes_to_numpy(attributes)
        self.targets = targets
        self.cnt_file_names = cnt_file_names_to_save
        self.ev2_file_names = ev2_file_names_to_save
        self.ev2_file_row_indices = ev2_file_row_indices

    @staticmethod
    def get_column_number(process_function, first_row):
        result = process_function(first_row)
        column_number = result.shape[0]
        return column_number

    @staticmethod
    def zip_attributes_to_numpy(attributes):

        row_num = sum(map(len, attributes))
        col_num = len(attributes[0][0])
        joined_attributes = np.empty(shape=(row_num, col_num))

        i = 0
        for target_level in attributes:
            for file_level in target_level:
                joined_attributes[i, :] = file_level
                i += 1

        return joined_attributes

    @property
    def data_set_df(self):
        data = {'attributes': self.attributes.tolist(),
                'targets': self.targets,
                'ev2_file_names': self.ev2_file_names,
                'ev2_file_row_indices': self.ev2_file_row_indices,
                'cnt_file_names': self.cnt_file_names}

        df = pd.DataFrame(data)
        return df

    def attributes_to_pdf(self):

        unique_targets = self.data_set_df.targets.unique()

        for act_target in unique_targets:
            act_pdf_file_name = act_target + ".pdf"
            act_target_data_set = self.data_set_df.loc[self.data_set_df.targets == act_target]

            with PdfPages(act_pdf_file_name) as pages:
                for index, row in act_target_data_set.iterrows():
                    fig = Figure()
                    act_title = row.cnt_file_names + "\n" + row.ev2_file_names + "\nev2_file_row_index: " + str(row.ev2_file_row_indices)
                    fig.suptitle(act_title, fontsize=10)
                    ax = fig.gca()
                    ax.set_xlabel("time (s)")
                    ax.set_ylabel("intensity (eV)")
                    ax.plot((np.arange(len(row.attributes)) / self.frequency), row.attributes)
                    canvas = FigureCanvasPdf(fig)
                    canvas.print_figure(pages)

        return None
