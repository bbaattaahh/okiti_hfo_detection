import pandas as pd
import re


def get_ev2_files_diff(ev2_1, ev2_2, result_file_name):
    ev2_1_df = read_ev2(ev2_1)
    ev2_2_df = read_ev2(ev2_2)

    ev2_1_df = add_id(ev2_1_df)
    ev2_2_df = add_id(ev2_2_df)

    ids_to_keep = pd.Series(list(set(ev2_1_df["id"]) - set(ev2_2_df["id"])))

    ev2_diff = ev2_1_df[ev2_1_df['id'].isin(ids_to_keep)][['index', 'channel', 'uc1', 'uc2', 'uc3', 'point_of_time']]

    ev2_diff.to_csv(result_file_name, sep=" ", header=None, index=None)

    return True


def add_id(df):
    df['id'] = df["channel"].map(str) + "_" + df["point_of_time"].map(str)
    return df


def read_ev2(file_name):
    rows = []

    with open(file_name) as f:
        for line in f:
            processed_line = line.strip()
            processed_line = re.sub(' +', ' ', processed_line)
            splitted_line = processed_line.split(" ")
            rows.append(splitted_line)

    df = pd.DataFrame(rows, columns=['index', 'channel', 'uc1', 'uc2', 'uc3', 'point_of_time'])
    return df
