from src.get_peaks import get_peaks


def get_peak_number(eeg_part, threshold_function):
    threshold = threshold_function(eeg_part)

    peaks = get_peaks(eeg_part, threshold)

    return len(peaks)