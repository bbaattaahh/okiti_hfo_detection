class Peak:
    def __init__(self,
                 start_index,
                 end_index,
                 values,
                 threshold):

        self.start_index = start_index
        self.end_index = end_index
        self.values = values
        self.threshold = threshold

        self.max_value = max(self.values)
        self.length = self.end_index - self.start_index