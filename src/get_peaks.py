from src.Peak import Peak


def get_peaks(eeg_part, threshold):

    peaks = []
    in_peak = False

    for i, value in enumerate(eeg_part):
        if value > threshold and in_peak is False:
            start_index = i
            in_peak = True

        if value <= threshold and in_peak is True:
            end_index = i
            in_peak = False
            act_peak = Peak(start_index,
                            end_index,
                            values=eeg_part[start_index:end_index],
                            threshold=threshold)
            peaks.append(act_peak)

    return peaks