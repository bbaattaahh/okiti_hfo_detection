import pywt
import itertools
import scipy.signal
from src.fraquency2scale import frequency2scale
import matplotlib.pyplot as plt
import numpy as np


def get_band_power_ratio(frequency_band_1, frequency_band_2, signal, sampling_period, wavelet='morl'):

    band_1_frequencies = list(range(frequency_band_1[0], frequency_band_1[1]))
    band_2_frequencies = list(range(frequency_band_2[0], frequency_band_2[1]))
    frequencies = band_1_frequencies + band_2_frequencies

    scales = list(map(frequency2scale,
                 frequencies,
                 itertools.repeat(wavelet, len(frequencies),),
                 itertools.repeat(sampling_period, len(frequencies))))

    cwtmatr, freqs = pywt.cwt(signal, scales, wavelet, sampling_period)

    power_spertral_density = np.zeros(cwtmatr.shape)
    for i, spectrum in enumerate(cwtmatr):
        power_spectrum = abs(spectrum)**2

        power_spertral_density[i, :] = power_spectrum

    power_spectral_density_band_1 = power_spertral_density[:len(band_1_frequencies)]
    power_spectral_density_band_2 = power_spertral_density[len(band_1_frequencies):]

    psd_band_1_max = np.amax(power_spectral_density_band_1)
    psd_band_2_max = np.amax(power_spectral_density_band_2)

    band_power_ratio = psd_band_1_max / psd_band_2_max

    return band_power_ratio
