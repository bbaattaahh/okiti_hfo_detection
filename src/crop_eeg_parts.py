from src.get_event_positions import get_event_positions


def crop_eeg_parts(event_positions, numpy_cnt, window_size):

    eeg_parts = list()

    if _is_it_ev2_file(event_positions):
        event_positions = get_event_positions(event_positions)
    else:
        event_positions = event_positions

    for event_position in event_positions:
        channel_index = event_position[0]

        column_index = event_position[1]

        lower_index = int(column_index - (window_size // 2))
        upper_index = int(column_index + (window_size // 2)) + 1

        if lower_index >= 0 and upper_index < numpy_cnt.shape[1]:
            act_eeg_part = numpy_cnt[channel_index][lower_index:upper_index]
            eeg_parts.append(act_eeg_part)

    return eeg_parts


def _is_it_ev2_file(file_name):
    if type(file_name) is str and file_name[-4:] == ".ev2":
        return True
    return False
