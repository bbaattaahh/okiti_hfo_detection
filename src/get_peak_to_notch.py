import pywt
import itertools
import numpy as np
from src.fraquency2scale import frequency2scale
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema


def get_peak_to_notch(signal, frequency_band, wavelet, sampling_period, search_frequency_start=80, search_window_size=50):

    frequencies = list(range(frequency_band[0], frequency_band[1]))

    scales = list(map(frequency2scale,
                 frequencies,
                 itertools.repeat(wavelet, len(frequencies),),
                 itertools.repeat(sampling_period, len(frequencies))))

    cwtmatr, freqs = pywt.cwt(signal, scales, wavelet, sampling_period)

    power_spertral_density = np.zeros(shape=cwtmatr.shape)

    for i, spectrum in enumerate(cwtmatr):
        power_spectrum = abs(spectrum)**2

        power_spertral_density[i, :] = power_spectrum

    avg_power_spectral_density = np.mean(power_spertral_density, axis=1)

    min_places = argrelextrema(avg_power_spectral_density, np.greater)[0]
    min_freqs = freqs[min_places]
    filtered_min_freqs = min_freqs[min_freqs >= search_frequency_start]

    filtered_min_places = min_places[-len(filtered_min_freqs):]

    peak_to_notch = -np.Inf

    for min_place in filtered_min_places:
        freq_to = freqs[min_place]
        freq_from = freqs[min_place] - search_window_size

        local_max = avg_power_spectral_density[freqs == freq_to][0]

        x = np.logical_and(freqs >= freq_from, freqs <= freq_to)
        avg_power_spectral_density_part = avg_power_spectral_density[x]

        local_min = min(avg_power_spectral_density_part)

        act_peak_to_notch = local_max / local_min

        if act_peak_to_notch > peak_to_notch:
            peak_to_notch = act_peak_to_notch

    return peak_to_notch
