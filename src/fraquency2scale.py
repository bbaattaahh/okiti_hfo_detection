import pywt


def frequency2scale(frequency, wavelet, sampling_period):

    central_frequency = pywt.central_frequency(wavelet, precision=8)

    scale = central_frequency / (frequency * sampling_period)

    return scale

