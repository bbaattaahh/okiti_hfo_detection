def get_event_positions(ev2_file_name):
    event_positions = list()

    f = open(ev2_file_name)

    while True:
        line = f.readline()

        if line == '':
            break

        line_split = line.split()

        actual_event_row_index = _matlab_index_to_python_index(int(line_split[1]))

        actual_event_column_index = _matlab_index_to_python_index(int(line_split[5]))

        event_positions.append((actual_event_row_index, actual_event_column_index))

    f.close()

    return event_positions


def _matlab_index_to_python_index(matlab_index):
    python_index = matlab_index - 1
    return python_index
