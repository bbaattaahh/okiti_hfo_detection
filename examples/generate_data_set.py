import json
import pickle
import scipy

from collections import OrderedDict

from src.DataSetGenerator import DataSetGenerator

data_set_map = json.loads(
    """{"./test/data/HFO_minta_block.cnt": {
            "positive": ["./test/data/HFO_minta_block_small_pos.ev2",
                          "./test/data/HFO_minta_block_small_pos_2.ev2"],
            "negative": ["./test/data/HFO_minta_block_small_neg.ev2"]
            }
    }""",
    object_pairs_hook=OrderedDict)


def process_cnt(raw_cnt):
    raw_cnt.filter(80, 500, fir_design='firwin')
    return raw_cnt


def process_whole_channel(x):
    smoothed_x = scipy.signal.savgol_filter(x, 51, 3)
    return smoothed_x


def process_eeg_part(x):
    return abs(x)

dsg = DataSetGenerator(window_size=501,
                       data_set_map=data_set_map,
                       cnt_process_function=process_cnt,
                       whole_channel_process_function=process_whole_channel,
                       eeg_parts_process_function=process_eeg_part)

dsg.generate_data_set()
dsg.attributes_to_pdf()

print(dsg.data_set_df)

pickle.dump(dsg, open("my_dsg.p", "wb"))


my_old_dsg = pickle.load(open("my_dsg.p", "rb"))

print(1)