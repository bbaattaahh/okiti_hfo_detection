import numpy as np
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import FigureCanvasPdf, PdfPages
from matplotlib.figure import Figure

from mne.io.cnt import read_raw_cnt
from okiti_cnt_to_cnt import okiti_cnt_to_cnt

from src.crop_eeg_parts import crop_eeg_parts
from matplotlib.backends.backend_pdf import PdfPages


okiti_cnt_to_cnt("data/HFO_minta_block.cnt", "data/mne_HFO_minta_block.cnt")
raw_cnt = read_raw_cnt("data/mne_HFO_minta_block.cnt", montage=None, preload=True)
eeg_data_as_numpy_array = raw_cnt.get_data()



print(raw_cnt.info["sfreq"])
print(raw_cnt.info)

# # before filter
# raw_cnt.plot()
# plt.show()

# after filter
raw_cnt.filter(80, 500, fir_design='firwin')
# raw_cnt.plot()
# plt.show()

eeg_data_as_numpy_array = raw_cnt.get_data()

eeg_parts = crop_eeg_parts("data/HFO_minta_block_altalam.ev2", eeg_data_as_numpy_array, 501)


with PdfPages('foo.pdf') as pages:
    for eeg_part in eeg_parts:
        fig = Figure()
        ax = fig.gca()
        ax.plot(np.arange(len(eeg_part)), eeg_part)
        canvas = FigureCanvasPdf(fig)
        canvas.print_figure(pages)
