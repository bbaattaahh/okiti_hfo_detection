import matplotlib.pyplot as plt

from mne.io.cnt import read_raw_cnt
from okiti_cnt_to_cnt import okiti_cnt_to_cnt

from src.crop_eeg_parts import crop_eeg_parts


okiti_cnt_to_cnt("data/HFO_minta_block.cnt", "data/mne_HFO_minta_block.cnt")
raw_cnt = read_raw_cnt("data/mne_HFO_minta_block.cnt", montage=None)
eeg_data_as_numpy_array = raw_cnt.get_data()

eeg_parts = crop_eeg_parts("data/HFO_minta_block_altalam.ev2", eeg_data_as_numpy_array, 1001)

plt.plot(eeg_parts[2])
plt.show()
