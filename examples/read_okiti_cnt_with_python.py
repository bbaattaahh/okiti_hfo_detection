import matplotlib.pyplot as plt

from mne.io.cnt import read_raw_cnt

from okiti_cnt_to_cnt import okiti_cnt_to_cnt

# Create mne-python compatible cnt. If mne_cnt.cnt file exist it is overwritten.
okiti_cnt_to_cnt("./data/okiti_cnt.cnt", "./data/mne_cnt.cnt")


# read mne compatible cnt
raw_cnt = read_raw_cnt("./data/mne_cnt.cnt", montage=None)


# print raw EEG data
eeg_data_as_numpy_array = raw_cnt.get_data()
print(eeg_data_as_numpy_array)


# visualize EEG data
raw_cnt.plot()
plt.show()
