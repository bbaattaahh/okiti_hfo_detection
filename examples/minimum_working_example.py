import json
import scipy
import itertools
import pandas as pd
import numpy as np
import pickle

from collections import OrderedDict

from src.get_ev2_files_difference import get_ev2_files_diff
from src.DataSetGenerator import DataSetGenerator


def process_cnt(raw_cnt):
    raw_cnt.filter(80, 500, fir_design='firwin')
    return raw_cnt


def process_whole_channel(x):
    smoothed_x = scipy.signal.savgol_filter(x, 51, 3)
    return smoothed_x


def process_eeg_part(x):
    return abs(x)


get_ev2_files_diff("./data/testdata/minta38_putative.ev2",
                   "./data/testdata/minta38_manualis.ev2",
                   "./data/testdata/minta38_negativ.ev2")


data_set_map = json.loads(
    """{"./data/testdata/minta38.cnt": {
            "positive": ["./data/testdata/minta38_manualis.ev2"],
            "negative": ["./data/testdata/minta38_negativ.ev2"]
            }
    }""",
    object_pairs_hook=OrderedDict)


dsg_processed = DataSetGenerator(window_size=401,
                                 data_set_map=data_set_map,
                                 cnt_process_function=process_cnt,
                                 whole_channel_process_function=process_whole_channel,
                                 eeg_parts_process_function=process_eeg_part)

dsg_processed.generate_data_set()

df = dsg_processed.data_set_df


def my_dummy_feature(eeg_part):
    return max(eeg_part)


df['dummy_feature'] = df.attributes.apply(my_dummy_feature)

with open("./examples/temp_minimum_working_data_set.pkl", 'wb') as f:
    pickle.dump(df, f)

df.to_csv("./examples/temp_minimum_working_data_set.csv")
