import json
import scipy
import itertools
import pandas as pd
import numpy as np
import pickle


from collections import OrderedDict
from matplotlib.backends.backend_pdf import PdfPages

from src.get_ev2_files_difference import get_ev2_files_diff
from src.DataSetGenerator import DataSetGenerator
from src.get_peak_number import get_peak_number
from src.get_peak_maximums_avg import get_peak_maximums_avg
from src.get_band_power_ratio import get_band_power_ratio
from src.get_peak_to_notch import get_peak_to_notch
from src.bin_features import bin_features

WINDOW_SIZE = 401


def process_cnt(raw_cnt):
    raw_cnt.filter(80, 500, fir_design='firwin')
    return raw_cnt


def process_whole_channel(x):
    smoothed_x = scipy.signal.savgol_filter(x, 51, 3)
    return smoothed_x


def process_eeg_part(x):
    return abs(x)


get_ev2_files_diff("./data/testdata/minta38_putative.ev2",
                   "./data/testdata/minta38_manualis.ev2",
                   "./data/testdata/minta38_negativ.ev2")

get_ev2_files_diff("./data/testdata/minta37_putative.ev2",
                   "./data/testdata/minta37_manualis.ev2",
                   "./data/testdata/minta37_negativ.ev2")


data_set_map = json.loads(
    """{"./data/testdata/minta38.cnt": {
            "positive": ["./data/testdata/minta38_manualis.ev2"],
            "negative": ["./data/testdata/minta38_negativ.ev2"]
            },
        "./data/testdata/minta37.cnt": {
            "positive": ["./data/testdata/minta37_manualis.ev2"],
            "negative": ["./data/testdata/minta37_negativ.ev2"]
            }
    }""",
    object_pairs_hook=OrderedDict)


dsg_raw = DataSetGenerator(window_size=WINDOW_SIZE,
                           data_set_map=data_set_map)


dsg_raw.generate_data_set()


dsg_processed = DataSetGenerator(window_size=WINDOW_SIZE,
                                 data_set_map=data_set_map,
                                 cnt_process_function=process_cnt,
                                 whole_channel_process_function=process_whole_channel,
                                 eeg_parts_process_function=process_eeg_part)


dsg_processed.generate_data_set()
# dsg_processed.attributes_to_pdf()

df_raw = dsg_raw.data_set_df

df = dsg_processed.data_set_df

df['f1_max'] = df.attributes.apply(max)

df['f2_std'] = df.attributes.apply(np.std)

df['f3_entropy'] = df.attributes.apply(scipy.stats.entropy)

bin_features(df['f3_entropy'], dsg_processed.targets)


df['f4_peak_number'] = pd.Series(map(get_peak_number,
                                     df.attributes,
                                     itertools.repeat(lambda x: 3*np.std(x), len(df.attributes))))

df['f5_peak_maximums_avg'] = pd.Series(map(get_peak_maximums_avg,
                                       df.attributes,
                                       itertools.repeat(lambda x: 3*np.std(x), len(df.attributes))))

df["f6_power_band_ratio_50_150_VS_500_600"] = pd.Series(map(get_band_power_ratio,
                                                         itertools.repeat(range(50, 150), len(df_raw.attributes)),
                                                         itertools.repeat(range(500, 600), len(df_raw.attributes)),
                                                         df_raw.attributes,
                                                         itertools.repeat(1/2000, len(df_raw.attributes)),
                                                         itertools.repeat("cmor", len(df_raw.attributes))))

df["f7_peak_to_notch"] = pd.Series(map(get_peak_to_notch,
                                       df_raw.attributes,
                                       itertools.repeat((40, 500), len(df_raw.attributes)),
                                       itertools.repeat("cmor", len(df_raw.attributes)),
                                       itertools.repeat(1/2000, len(df_raw.attributes))))


plot_dfs = []
pp = PdfPages('target_value_distribution_in_bins_of_features.pdf')

for feature in ['f1_max','f2_std', 'f3_entropy', 'f4_peak_number', 'f5_peak_maximums_avg', "f6_power_band_ratio_50_150_VS_500_600", "f7_peak_to_notch"]:
    act_df = bin_features(df[feature], dsg_processed.targets)

    act_df.plot(x="bin_label", y=["neg", "pos"], kind="bar", title=feature)
    pp.savefig()

pp.close()

with open("data_set.pkl", 'wb') as f:
    pickle.dump(df, f)
